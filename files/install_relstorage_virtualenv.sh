#!/bin/bash

PYTHON="/usr/local/bin/python3.6"
PATH="$1"
PIP="$PATH/bin/pip"

$PYTHON -m venv "$PATH"
$PIP install "ZODB>=4,<6" "zodburi" "RelStorage==3.0.*" "psycopg2-binary==2.8.*"
