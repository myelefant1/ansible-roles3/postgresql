# coding: utf-8

import sys

from ZODB.DB import DB
import zodburi


def main(uri):
    storage_factory, dbkw = zodburi.resolve_uri(uri)
    storage = storage_factory()
    db = DB(storage, **dbkw)
    print(db, len(storage))


if __name__ == "__main__":
    main(*sys.argv[1:])
