## Postgresql

This role installs and configures Postgresql.

Tested on debian 10 & 12

## Role parameters

| name                        | value    | optionnal | default value   | description                              |
| ----------------------------|----------|-----------|-----------------|------------------------------------------|
| postgresql_cluster_name     | string   | yes       | shortener       | name of cluster |
| pg_data                     | string   | yes       |  /srv/pgdata    | where data are stored |
| pg_archive                  | string   | yes       | /srv/pgarchive  | where archive are stored |
| pg_log                      | string   | yes       | /srv/pgloghive  | where log     are stored |
| pg_version                  | number   | yes       | 11 ||
| pg_shared_buffers           | string   | yes       | '65536 MB' | see postgresql documentation |

postgresql_conf_dir: "{{ pg_data }}/postgresql/{{ pg_version }}/{{ postgresql_cluster_name }}"
postgresql_allowed_clients: []

## Using this role

### ansible galaxy

Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/postgresql.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: postgresql
```

## Tests

[tests/tests_postgresql](tests/tests_postgresql)
---
